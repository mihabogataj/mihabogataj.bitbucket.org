var openWeatherAPI = "https://api.openweathermap.org/data/2.5/weather?q={Kraj}&appid=2831ce0df256879b5953a8640fc1b0e4";

function pridobiLokacijo() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        
    }
}

function pridobiOkolico() {
    document.getElementById("dodajTemperaturoOkolice").value = null;
    document.getElementById("dodajVlaznost").value = null;
    
    var kraj = document.getElementById("dodajLokacijo").value;
    var requestString = String(openWeatherAPI).replace("{Kraj}", kraj);
    
    /*var response = $.ajax({
        type: "GET",
        url: requestString,
        crossdomain: true,
        async: false
    });
    
    console.log(response.responseJSON.main);
    
    document.getElementById("dodajTemperaturoOkolice").value = parseFloat(response.responseJSON.main.temp - 273.15).toFixed(2);
    document.getElementById("dodajVlaznost").value = response.responseJSON.main.humidity;*/
    
    fetch(requestString).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById("dodajTemperaturoOkolice").value = parseFloat(json.main.temp - 273.15).toFixed(2);
        document.getElementById("dodajVlaznost").value = json.main.humidity;
        console.log(json);
    });

}
