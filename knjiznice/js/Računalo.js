var conductivity = 0.24825; // W/mK
var skin_tickness = 0.0025; //m
var specificBodyHeat = 3500; //J/kgK
var dt = 10; //s - check temp each second
var max_time = 18000;
var surface;

function racunaj() {
    if (document.getElementById("dodajTemperaturo").value.length != 0 &&
        document.getElementById("dodajVisino").value.length != 0 &&
        document.getElementById("dodajTelesnoTezo").value.length != 0 &&
        document.getElementById("dodajTemperaturoOkolice").value.length != 0 &&
        document.getElementById("dodajVlaznost").value.length != 0
    ) {
        document.getElementById("messageCalculator").innerHTML = "Računam!";
        
        var weight = document.getElementById("dodajTelesnoTezo").value;
        var height = document.getElementById("dodajVisino").value;
        var body_temp = parseFloat(document.getElementById("dodajTemperaturo").value) + 273.15; //K
        var outdoor_temp = parseFloat(document.getElementById("dodajTemperaturoOkolice").value) + 273.15; //K
        
        //Haycock formula for body surface calculation
        surface = 0.024265 * Math.pow(weight, 0.5378) * Math.pow(height, 0.3964);
        
        //Human skin conductivity = 0.25825 W/mK
        //Human skin tickness = 2-3 mm
        //Average radiation of an average human body = 96.88 W/m^2K - will not be used
        //Average human body surface area = 1.9m^2 - will not be used
        //Average human body radiation per square meter = 51 W/m^2 - will not be used
        //Heat flux: j=-λ(ΔT/l); W/m^2 - used due to usage of body temperature and outdoor temperature
        //Specific heat of human body: c=3500J/kgK
        
        //var radiation = 51 * surface; // W/m^2 for given body - probably won't be used
        
        var j = (-1) * conductivity * ((body_temp - outdoor_temp) / (skin_tickness));     //W/m^2
        var P = j * surface;
        
        var tempByTime = [];
        var timeData = [];
        
        tempByTime.push(parseFloat(parseFloat(body_temp - 273.15).toFixed(2)));
        timeData.push("0s");
        
        console.log("start");
        setTimeout(null, 1);

        var current_time = 0;
        while (current_time <= max_time && body_temp >= 295.15 && body_temp <= 316.15) {
            current_time += dt;
            var dT = izracunajSprememboTemperature(body_temp, outdoor_temp, dt, weight);
            body_temp = body_temp + dT;
            
            tempByTime.push(parseFloat(parseFloat(body_temp - 273.15).toFixed(2)));
            timeData.push(current_time + "s");
            
            setTimeout(null, 1);
            console.log("Body temperature: " + body_temp + ", change: " + dT + ", after " + current_time + " seconds.");
        }
        
        if (current_time >= max_time) document.getElementById("messageCalculator").innerHTML = "Vremenske pogoje bi lahko preživeli!";
        else document.getElementById("messageCalculator").innerHTML = "V teh vremenskih pogojih bi lahko preživeli do " + current_time + " sekund!";
        
        document.getElementById("graf").innerHTML = "";
        displayChart(tempByTime, timeData);
        
        console.log("stop");
        //console.log("Temp rad: " + P + ", radiation: " + radiation + ", surface: " + surface);
    }
    else {
        document.getElementById("messageCalculator").innerHTML = "Manjkajoča vnosna polja!";
    }
}

function izracunajSprememboTemperature(body_temp, outdoor_temp, dt, weight) {
    var j = (-1) * conductivity * ((body_temp - outdoor_temp) / (skin_tickness));
    var P = j * surface;
    
    var dT= (P * dt) / (weight * specificBodyHeat);
    return dT;
}

function displayChart(tempByTime, timeData) {
    $("#graf").highcharts({
        title: {
            text: "Graf telesne temperature",
            x: -20
        },
        chart: {
            height: 250
        },
        xAxis: {
            categories: timeData
        },
        yAxis: {
            title: {
                text: "°C"
            },
            plotLines: [{
                value: 10,
                width: 1,
                color: "#808080"
            }],
            alignTicks: false,
            tickInterval: dt,
            units: [
                "s", 
                [10, 20, 30, 40, 50, 60]
            ]
        },
        tooltip: {
            valueSuffix: "°C"
        },
        legend: {
            layout: "vertical",
            align: "right",
            verticalAlign: "middle",
            borderWidth: 0
        },
        series: [
        	{
            	name: "Telesna temperatura",
            	data: tempByTime
        	}
        ]
    });
}