var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 function generirajVsePodatke() {
    console.log("Kreiram vse podatke!");
    sessionId = getSessionId();
     
    generirjEHR1();
    generirjEHR2();
    generirjEHR3();
     
    setTimeout(function() {
    	$("#statusZahteve").html("<span class='obvestilo " + "label label-success fade-in'>Uspešno kreiran vsi podatki!</span>");
	
	    var e = document.getElementById("selUporabniki");
		var value = e.options[e.selectedIndex].value;
		preberiVitalneZnake(value);
    }, 500);
    
	
 }


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function kreirajEHRzaBolnika() {
    console.log("Kreiram EHR!");
    
    var kreiraj = document.getElementById("preberiObstojeciEHR").value;
    
	sessionId = getSessionId();

    
    if (kreiraj == 1) {
        //1
    	generirjEHR1();
    }
	else if (kreiraj == 2) {
	    //2
    	generirjEHR2();
	}
	else if (kreiraj == 3) {
	    //3
    	generirjEHR3();
	}
	
}

function generirjEHR1() {
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: "Janez",
		        lastNames: "Kranjski",
		        dateOfBirth: "1992-05-26T21:36Z",
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    $("#statusZahteve").html("<span class='obvestilo " + "label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
	                    $("#preberiEHRid").val(ehrId);
	                    $("#selUporabniki").append('<option value="' + ehrId + '">Janez Kranjski</option>');
	                }
	                
	                $.ajaxSetup({
                        headers: {"Ehr-Session": sessionId}
                    });
                    var podatki = {
                    	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
                    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                        "ctx/language": "en",
                        "ctx/territory": "SI",
                        "vital_signs/height_length/any_event/body_height_length": "218",
                        "vital_signs/body_weight/any_event/body_weight": "92",
                       	"vital_signs/body_temperature/any_event/temperature|magnitude": "36.6",
                        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                    };
                    var parametriZahteve = {
                        ehrId: ehrId,
                        templateId: 'Vital Signs',
                        format: 'FLAT',
                        committer: "User"
                    };
                    $.ajax({
                        url: baseUrl + "/composition?" + $.param(parametriZahteve),
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(podatki),
                        success: function (res) {
                            $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
                        },
                        error: function(err) {
                        	$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='statusZahteve label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
	            },
	            error: function(err) {
	            	$("#statusZahteve").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	            }
	        });
	    }
	});
}

function generirjEHR2() {
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: "Marjana",
		        lastNames: "Ljubljanska",
		        dateOfBirth: "1988-12-02T21:36Z",
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    $("#statusZahteve").html("<span class='obvestilo " + "label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
	                    $("#preberiEHRid").val(ehrId);
	                    $("#selUporabniki").append('<option value="' + ehrId + '">Marjana Ljubljanska</option>');
	                }
	                
	                $.ajaxSetup({
                        headers: {"Ehr-Session": sessionId}
                    });
                    var podatki = {
                    	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
                    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                        "ctx/language": "en",
                        "ctx/territory": "SI",
                        "vital_signs/height_length/any_event/body_height_length": "145",
                        "vital_signs/body_weight/any_event/body_weight": "110",
                       	"vital_signs/body_temperature/any_event/temperature|magnitude": "38",
                        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                    };
                    var parametriZahteve = {
                        ehrId: ehrId,
                        templateId: 'Vital Signs',
                        format: 'FLAT',
                        committer: "User"
                    };
                    $.ajax({
                        url: baseUrl + "/composition?" + $.param(parametriZahteve),
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(podatki),
                        success: function (res) {
                            $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
                        },
                        error: function(err) {
                        	$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='statusZahteve label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
	            },
	            error: function(err) {
	            	$("#statusZahteve").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	            }
	        });
	    }
	});
}

function generirjEHR3() {
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: "Jože",
		        lastNames: "Mariborski",
		        dateOfBirth: "1963-01-02T21:36Z",
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    $("#statusZahteve").html("<span class='obvestilo " + "label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
	                    $("#preberiEHRid").val(ehrId);
	                    $("#selUporabniki").append('<option value="' + ehrId + '">Jože Mariborski</option>');
	                }
	                
	                $.ajaxSetup({
                        headers: {"Ehr-Session": sessionId}
                    });
                    var podatki = {
                    	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
                    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                        "ctx/language": "en",
                        "ctx/territory": "SI",
                        "vital_signs/height_length/any_event/body_height_length": "186",
                        "vital_signs/body_weight/any_event/body_weight": "60",
                       	"vital_signs/body_temperature/any_event/temperature|magnitude": "35",
                        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                    };
                    var parametriZahteve = {
                        ehrId: ehrId,
                        templateId: 'Vital Signs',
                        format: 'FLAT',
                        committer: "User"
                    };
                    $.ajax({
                        url: baseUrl + "/composition?" + $.param(parametriZahteve),
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(podatki),
                        success: function (res) {
                            $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
                        },
                        error: function(err) {
                        	$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='statusZahteve label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
	            },
	            error: function(err) {
	            	$("#statusZahteve").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	            }
	        });
	    }
	});
}

function preberiEHR(ehrId) {
    sessionId = getSessionId();
    
	if (!ehrId || ehrId.trim().length == 0) {
		$("#messageCalculator").html("<span class='obvestilo label label-warning " + "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
			},
			error: function(err) {
				$("#messageCalculator").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function preberiVitalneZnake(ehrId) {
    sessionId = getSessionId();
    
    var temp;
    var weight;
    var height;
    
    if (ehrId.length === 0) return;
    else {
        $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				//Telesna temperatura
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
					        temp = res[0].temperature;
					        document.getElementById("dodajTemperaturo").value = temp;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html();
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
				
				//Telesna teza
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "weight",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
				    	    weight = res[0].weight;
				    	    document.getElementById("dodajTelesnoTezo").value = weight;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>" + "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
				
				//Telesna visina
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "height",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
				    	    height = res[0].height;
				    	    document.getElementById("dodajVisino").value = height;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>" + "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
    }
}

function onSelectChanged() {
	var e = document.getElementById("selUporabniki");
	var value = e.options[e.selectedIndex].value;
	preberiVitalneZnake(value);
}